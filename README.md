# Profils QGIS pour OCHARY

Cet entrepôt contient les profils QGIS utilisés à Charente Eaux Rivière.

# Pour préparer l'installation avec le .exe

### La modification des profils nécessaire :

- Supprimer le fichier *qgis-auth*. (Il conteint des mot de passe personnalisés par l'utilisateur)
- Dans *\ocarhy_terrain\QGIS\QGIS3.ini* ; S'assurer de la bonne configuration de l'extension *layers menu from project*, et ajouter les balises splash mentionnées dans le .exe
- Dans *\ocarhy_terrain\QGIS\QGISCUSTOMIZATION3.ini* ; S'assurer que la deuxième ligne soit égale à : ```splashpath={{ SPLASH_DIR }}```

### L'archivage pour l'envoi aux utilisateurs

Il faut zipper les deux profils ainsi que les fichiers annexes (*qgis_global_settings.ini*,*startup.py*...) dans un document intitulé *Deploiement.zip*. Cette archive est à déposer à la racine de l'installateur .exe

# Autres éléments...

## Script de démarrage de QGIS

Script Python appelé à la fin du chargement du coeur de QGIS et de son API Python (PyQGIS) dont le chemin est paramétrable via la variable d'environnement : `PYQGIS_STARTUP`.

### Développement

Installer les *git hooks* pour prévenir plutôt que guérir :

```bash
pip install pre-commit
pre-commit install
```
