<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>FrenchBanGeocoderLocatorFilter</name>
    <message>
        <location filename="../../core/locator_filter.py" line="68"/>
        <source>French Adress geocoder</source>
        <translation>Géocodeur API BAN</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="199"/>
        <source>Result triggerred by the user received: {}</source>
        <translation type="obsolete">Résultat sélectionné par l&apos;utilisateur reçu : {}</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="208"/>
        <source>Something went wrong during result deserialization: {}. Trying another method...</source>
        <translation type="obsolete">Impossible de récupérer le résultat sélection : {}. Essai par une autre méthode...</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="142"/>
        <source>API search not triggered. Reason: </source>
        <translation type="obsolete">Recherche API non déclenchée. Raison : </translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="120"/>
        <source>minimum chars {} not reached: {}</source>
        <translation type="obsolete">nombre minimum de caractére {} non atteint : {}</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="133"/>
        <source>search term is matching the prefix.</source>
        <translation type="obsolete">le terme de recherche correspond au préfixe.</translation>
    </message>
    <message>
        <location filename="../../core/locator_filter.py" line="142"/>
        <source>Search term &apos;{}&apos; is one of special terms to be ignored.</source>
        <translation type="obsolete">le terme &apos;{}&apos; est l&apos;un des termes spéciaux à ignorer.</translation>
    </message>
</context>
<context>
    <name>dlg_settings</name>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="26"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="88"/>
        <source>Features</source>
        <translation>Fonctionnalités</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="156"/>
        <source>HTTP user-agent:</source>
        <translation>HTTP user-agent :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="320"/>
        <source>Request URL:</source>
        <translation>Base de l&apos;URL de requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="313"/>
        <source>Request parameters:</source>
        <translation>Paramètres de la requête :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="350"/>
        <source>Miscellaneous</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="366"/>
        <source>Enable debug mode.</source>
        <translation>Activer le mode debug.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="375"/>
        <source>Debug mode (degraded performances)</source>
        <translation>Activer le mode debug (performances dégradées)</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="359"/>
        <source>Version used to save settings:</source>
        <translation>Paramètres sauvegardés avec la version :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="397"/>
        <source>Report an issue</source>
        <translation>Signaler une anomalie</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="433"/>
        <source>Help</source>
        <translation>Aide en ligne</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="125"/>
        <source>Minimal search length</source>
        <translation>Nombre minimal de caractères avant de déclencher la requête à l&apos;API</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="131"/>
        <source> characters</source>
        <translation> caractères</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="211"/>
        <source>Minimal search length:</source>
        <translation>Longueur minimale de la recherche :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="185"/>
        <source>HTTP content type:</source>
        <translation>Type de contenu HTTP :</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="430"/>
        <source>Open documentation</source>
        <translation>Ouvrir l&apos;aide en ligne du plugin sur votre navigateur</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="233"/>
        <source>List of strings to ignore in search bar, separated by commas.</source>
        <translation>Liste de termes à ignorer dans la berre de recherche, séparés par des virgules.</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="440"/>
        <source>Reset to factory default</source>
        <translation>Réinitialise les paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="106"/>
        <source>Search terms to ignore:</source>
        <translation>Paramètres de recherche à ignorer:</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="252"/>
        <source>BAN API configuration</source>
        <translation>Configuration API BAN</translation>
    </message>
    <message>
        <location filename="../../gui/dlg_settings.ui" line="307"/>
        <source>Photon API configuration</source>
        <translation>Configuration API Photon</translation>
    </message>
</context>
</TS>
