[general]
name=French Locator Filter
category=Filter

about=The geocoding API is an open service provided and maintained by the French Government.
about[fr]=L'API du gouvernement français est un service de geocodage gratuit et ouvert
    base sur la Base Adresse Nationale (BAN). Ce plugin ajoute un filtre de recherche
    basée sur ce service dans la barre de recherche universelle (Localisateur).

description=Adds a geocoder to the QGIS Locator Filter, using the national French open address API.
description[fr]=Ajoute un filtre de géocodage basé sur l'API de la BAN dans le
    Localisateur (barre de recherche universelle) de QGIS.

icon=resources/images/icon.svg

tags=geocoder, locator, filter, addok, France, french
tags[fr]=geocodeur, recherche, adresse, France, ADDOK, Etalab, BAN, BANO

# credits and contact
author=Régis Haubourg (Oslandia), Richard Duivenvoorde, Julien Moura (Oslandia)
email=qgis@oslandia.com
homepage=https://oslandia.gitlab.io/qgis/french_locator_filter/
repository=https://gitlab.com/Oslandia/qgis/french_locator_filter/
tracker=https://gitlab.com/Oslandia/qgis/french_locator_filter/-/issues

# status
deprecated=False
experimental=False
qgisMinimumVersion=3.16
qgisMaximumVersion=3.99

# versioning
version=1.1.0
changelog=
 Version 1.1.0:
 - Add new locator with Photon API by Jean-Marie KERLOCH #19
 - Fix min search length by Rémi Desgrange - !27
 - Add help path and key to connect plugin documentation to standard help button in settings view by Julien Moura - !28

 Version 1.0.4:
 - Handle special search terms to API - #9
 - Reduce cases where a bug that can lead to a QGIS crash because of legacy method
 - Add a button to reset settings to factory default
 - Clean legacy code

 Version 1.0.3:
 - change minimal search length See #8 - !20 by @nicogodet
 - map report button to the bug issue template
 - prevent Sonar run on forks
 - dependencies update


