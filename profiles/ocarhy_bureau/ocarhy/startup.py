from qgis.core import (
    QgsApplication,
    QgsRasterLayer,
    QgsAuthMethodConfig,
    QgsDataSourceUri,
    QgsPkiBundle,
    QgsMessageLog,
    QgsCredentials
  )

from qgis.gui import (
      QgsAuthAuthoritiesEditor,
      QgsAuthConfigEditor,
      QgsAuthConfigSelect,
      QgsAuthSettingsWidget,
  )

from qgis.PyQt.QtWidgets import (
      QWidget,
      QTabWidget,
  )

authMgr = QgsApplication.authManager()

if not authMgr.masterPasswordSame("qgis"):
        authMgr.setMasterPassword('qgis',True)
        
if authMgr.configIdUnique("OCARHY1"):
        uri = QgsDataSourceUri()
        uri.setConnection("141.95.145.201", "5432", "ocarhy", None, None)
        connInfo = uri.connectionInfo()
        (success, user, passwd) = QgsCredentials.instance().get(connInfo, None, None)
        uri.setPassword(passwd)
        uri.setUsername(user)

        input_user=uri.username()
        input_mdp=uri.password()

        authMgr = QgsApplication.authManager()
        config = QgsAuthMethodConfig()
        config.setId("OCARHY1")
        config.setName("OCARHY")
        config.setMethod("Basic")
        config.setConfig('username', input_user)
        config.setConfig('password', input_mdp)

        # check if method parameters are correctly set
        assert config.isValid()

        authMgr.storeAuthenticationConfig(config)
        newAuthCfgId = config.id()
        assert newAuthCfgId
        
 